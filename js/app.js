/*--Change tab in service--*/
let tabsTitlesService = document.getElementsByClassName('tabs__titles-service')[0];
let tabsContentService = document.getElementsByClassName('tabs__content-service')[0];

tabsTitlesService.addEventListener('click', function (event) {
   if (event.target.closest('.tab__title-service')) {
      let titles = Array.from(tabsTitlesService.children);
      let contents = Array.from(tabsContentService.children);

      titles.forEach(title => title.classList.remove('active-service-tab'));
      event.target.classList.add('active-service-tab');

      contents.forEach(tab => {
         if (tab.dataset.service === event.target.dataset.service) {
            tab.classList.add('active-service-tab');
         } else {
            tab.classList.remove('active-service-tab');
         }
      });
   }
});

/*--Change tab in work--*/
const tabsTitlesWork = document.querySelector('.tabs__titles-work');
const tabsContentWork = document.querySelector('.tabs__content-work');
let imgWorkCount = 1;
let itemsCount = 12;

tabsTitlesWork.addEventListener('click', addWorkItem);

function addWorkItem(event) {
   const itemType = event.target.dataset.work;
   Array.from(tabsTitlesWork.children).forEach(title => title.classList.remove('active-work-title'));
   event.target.classList.add('active-work-title');

   Array.from(tabsContentWork.children).forEach(tab => tab.classList.remove('active-work-tab'));

   if (itemType.toLowerCase() === 'all' && Array.from(tabsContentWork.children).length >= itemsCount) {
      for (let i = 0; i < itemsCount; i++) {
         Array.from(tabsContentWork.children)[i].classList.add('active-work-tab');
      }
   } else {
      if (itemType.toLowerCase() === 'all') {
         const firstItemType = tabsContentWork.querySelector('[data-work-item]').dataset.workItem;
         const arrWorkItems = tabsContentWork.querySelectorAll(`[data-work-item="${firstItemType}"]`);
         arrWorkItems.forEach(item => item.classList.add('active-work-tab'));

         if (arrWorkItems.length < itemsCount) {
            addNewWorkItems(firstItemType, arrWorkItems.length, itemsCount);
         }
      } else {
         const arrWorkItems = tabsContentWork.querySelectorAll(`[data-work-item="${itemType}"]`);
         arrWorkItems.forEach(item => item.classList.add('active-work-tab'));

         if (arrWorkItems.length < itemsCount) {
            addNewWorkItems(itemType, arrWorkItems.length, itemsCount);
         }
      }
   }
}

function addNewWorkItems(itemType, arrWorkItemsLength, itemsCount) {
   for (let i = arrWorkItemsLength; i < itemsCount; i++) {
      const randomImg = `https://picsum.photos/300/220?random=${imgWorkCount}`;
      tabsContentWork.insertAdjacentHTML("beforeend",
          `<div data-work-item="${itemType}" class="tab__item-work active-work-tab new-work-tab">
            <img src=${randomImg} alt="work example" />
            <div class="details__item-work">
               <div class="details__item-work-btns">
                  <button class="details__item-work-btn"></button>
                  <button class="details__item-work-btn"></button>
               </div>
               <h4>creative design</h4>
               <h5>${itemType}</h5>
            </div>
         </div>`);
      imgWorkCount++;
   }
}


/*--Btn that adds images in work--*/
let workBtn = document.querySelector('.work__btn');
let workBtnClickCount = 0;
let workBtnTimerId;

workBtn.onclick = function () {
   workBtn.setAttribute("disabled", '');
   workBtn.innerHTML = `<div class="btn__loader-container">
                           <span class="circle"></span>
                           <span class="circle"></span>
                           <span class="circle"></span>
                        </div>`;
   workBtn.style = "background-color: #14B9D5;"
   workBtnTimerId = setTimeout(() => {
      itemsCount += 12;
      document.querySelector('.active-work-title').click();
      if (workBtnClickCount === 1) {
         document.querySelector('.work').style = "padding-bottom: 99px;"
         workBtn.remove();
         clearTimeout(workBtnTimerId);
      }
      workBtnClickCount++
      workBtn.innerHTML = '<p>Load More</p>'
      workBtn.style = '';
      workBtn.removeAttribute("disabled");
   }, 2000);
};
/*--About Ham feedback slider--*/
const slides = document.querySelectorAll(".slide");
const thumbnails = document.querySelectorAll(".thumbnail");
const leftArrow = document.querySelector(".left-arrow");
const rightArrow = document.querySelector(".right-arrow");

let currentSlide = 0;
let currentActive = 0;
let testimTimer;

function playSlide(slide = currentSlide + 1) {
   for (const [slideIndex, slide] of slides.entries()) {
      slide.classList.remove("active");
      thumbnails[slideIndex].classList.remove("active");
   }

   if (slide < 0) {
      slide = currentSlide = slides.length - 1;
   }

   if (slide > slides.length - 1) {
      slide = currentSlide = 0;
   }

   slides[slide].classList.add("active");
   thumbnails[slide].classList.add("active");

   currentActive = currentSlide;

   clearTimeout(testimTimer);
   testimTimer = setTimeout(() => {
      playSlide(currentSlide += 1);
   }, 4000);
}

leftArrow.addEventListener("click", () => {
   playSlide(currentSlide -= 1);
});

rightArrow.addEventListener("click", () => {
   playSlide(currentSlide += 1);
});

for (const [thumbnailIndex, thumbnail] of thumbnails.entries()) {
   thumbnail.addEventListener("click", () => {
      playSlide(currentSlide = thumbnailIndex);
   });
}

playSlide();

